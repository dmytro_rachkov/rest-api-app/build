FROM python:3.9-slim AS builder
ENV PYTHONUNBUFFERED 1
WORKDIR /home/app
ENV PATH "/venv/bin:$PATH"
RUN python -m venv /venv
COPY ./application/requirements.txt .
RUN pip install -r requirements.txt

FROM python:3.9-alpine AS app
WORKDIR /home/app
ENV PATH "/venv/bin:$PATH"
COPY --from=builder /venv /venv
COPY . .
EXPOSE 8080
CMD ["python", "run.py"]
