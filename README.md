Table of Contents
-----------------
1. [Description](#description)
2. [Pipeline](#pipeline)
3. [Dockerfile and Smoke tests](#dockerfile-and-smoke-tests)
3. [Disclaimer](#disclaimer)
4. [License](#license)
5. [Author](#author-information)

Description
=========

Repo provides a sample RESTfull API application with flask frontend and mongodb backend. The build pipeline is creating and uploading docker image to the registry.

Application is serving HTTP RESTfull API endpoint to create, retrieve and delete sample user data (saved as documents in mongodb collection `user`).

Pipeline
------------

A multistage gitlab-ci pipeline is built using Kubernetes as executor and consists of:
- test stage: linting coding standards and syntax check (is going to be further enriched with unit tests with pytest)
- build stage: using kaniko and pushing the image to registry
- vulnerability scanning: using trivy static image scanner and giltab template to visualize in UI - the json artifact is available as well.
- static application vulnerabilities scanning:
  - [semgrep-test](https://github.com/marketplace/actions/semgrep-action): using native gitlab-ci integration with semgrep
  - [bandit-sast](https://pypi.org/project/bandit/): python static code vulnerability analysis


Dockerfile and Smoke tests
------------

In `test_dockerfile` job will run openpolicy/conftest image over `policy/docker.rego` policy file to assert the criteria, in current version of the test Dockerfile is instructed NOT to have `latest` tag, so the test will fail if tag is `latest`.  

In `smoke_test` pytest is used with conftest to run a smoke test over mockup flask application (from source code) and gitlab-ci container services mongodb-service is used as mockup persistence layer. The test itself if doing simple query to a flask route /users and asserts if HTTP response code is 200.

Disclaimer
-------

`docker-compose.yml` can be used for basic tests and quick setup in case one doesn't have Kubernetes to deploy the application. For k8s deployment, please consult `deploy` sister repository with automated CD pipeline using helm.

License
-------

GNU GPL license


Author Information
------------------

Dmitry Rachkov
