def test_index(app, client, monkeypatch):
    monkeypatch.setenv("mongodb-service", "mongodb-service")
    res = client.get("/v1/users")

    assert res.status_code == 200
