from .db import db
import mongoengine_goodjson as gj


class User(gj.Document):
    id = db.SequenceField(primary_key=True, required=True)
    name = db.StringField(required=True, unique=False, max_length=50)
