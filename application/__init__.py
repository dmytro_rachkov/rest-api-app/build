from .database.db import init_db
from .database.modeling import User
import json
from flask import Flask, request, Response, render_template
from .flask_extension.prefix import RootPrefix

app = Flask(__name__)
app.debug = True
app.wsgi_app = RootPrefix(app.wsgi_app, prefix='/v1')
app.config['MONGODB_SETTINGS'] = {
    'db': 'users',
    'host': 'mongodb://mongodb-service/users',
    'port': 27017
}
init_db(app)


@app.route('/')
def index():
    return render_template('index.html', mimetype="text/css", status=200)


@app.route('/users', methods=['GET'])
def get_users():
    users = User.objects().to_json()
    return Response(users, mimetype="application/json", status=200)


@app.route('/users/<id>', methods=['GET'])
def get_user(id):
    try:
        user = User.objects.get(id=id).to_json()
        return Response(user, mimetype="application/json", status=200)
    except User.DoesNotExist:
        return Response(
            str('User with this id does not exist'),
            mimetype="application/json",
            status=404)


@app.route('/users', methods=['POST'])
def add_user():
    body = request.get_json()
    user = User(**body).save()
    u = json.dumps({'id': int(user.id), 'name': str(user.name)})
    return Response(u, mimetype="application/json", status=200)


@app.route('/users/<id>', methods=['DELETE'])
def delete_user(id):
    try:
        User.objects.get(id=id).delete()
        return Response('', mimetype="application/json", status=200)
    except User.DoesNotExist:
        return Response(
            str('User with this id does not exist'),
            mimetype="application/json",
            status=404)
